# Changelog

## 1.1.1

**Visual changes:**
- VueTyper was removed -> logo on the main page is no longer animated

**Non-functional changes:**
- Project dependencies were updated (before it lead to failure in build process on Windows)
- vue-typer and axios were removed from dependencies
- Minor linting error corrections
