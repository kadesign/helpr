module.exports = {
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.devtool = 'nosources-source-map'
    } else {
      config.devtool = 'source-map'
    }
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false
    }
  },
  devServer: {
    port: 8800
  }
};
