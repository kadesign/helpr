export const tools = [
  {
    name: 'Object Definitions Checker',
    tags: ['BMC Remedy', 'ARS'],
    description: 'Check if you forgot to add necessary objects to your .def file',
    path: 'def-checker'
  },
  {
    name: 'ARX Viewer',
    tags: ['BMC Remedy', 'ARS'],
    description: 'View contents of AR export file in a proper way',
    path: 'arx-viewer'
  }
];
