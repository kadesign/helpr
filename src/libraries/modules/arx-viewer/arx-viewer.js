const regex = {
  schema: /SCHEMA "([^\n]*)"\r?\n/,
  fields: /FIELDS ([^\n]*)\r?\n/,
  fieldIds: /FLD-ID ([\d ]*)\r?\n/,
  dataTypes: /DTYPES ([A-Z ]*)\r?\n/,
  data: /DATA ([^\n]*)\r?\n/g
};

function formatEpochToDate (epoch) {
  const date = new Date(epoch * 1000);

  return ('0' + date.getUTCDate()).slice(-2) + '.' +
    ('0' + (date.getUTCMonth() + 1)).slice(-2) + '.' +
    date.getUTCFullYear() + ' ' +
    ('0' + date.getUTCHours()).slice(-2) + ':' +
    ('0' + date.getUTCMinutes()).slice(-2) + ':' +
    ('0' + date.getUTCSeconds()).slice(-2);
}

function parseArx (file) {
  const parsedArx = {
    schema: regex.schema.exec(file)[1],
    fields: [],
    data: []
  };

  // Extract column info
  const fieldStr = regex.fields.exec(file)[1];
  const fieldNames = /[^\s"]+|"([^"]*)"/g.matchAll(fieldStr, 1);
  const fieldIds = regex.fieldIds.exec(file)[1].split(' ');
  const dTypes = regex.dataTypes.exec(file)[1].split(' ');
  fieldNames.forEach((fld, i) => {
    parsedArx.fields.push({
      title: fld.trim(),
      field: fieldIds[i],
      formatter: 'plaintext',
      sorter: dTypes[i] === 'ENUM' || dTypes[i] === 'INTEGER' ? 'number' : 'string',
      headerTooltip: 'ID: ' + fieldIds[i] + '\nType: ' + dTypes[i]
    });
  });

  // Extract data
  const dataRaw = regex.data.matchAll(file, 1);
  dataRaw.forEach(entryStr => {
    entryStr = entryStr.replace(/(?<!\\)"{2}/g, 'null')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
    const data = {};
    const values = /"([^"\\]*(?:\\.[^"\\]*)*)"|[^\s"]+/g.matchAll(entryStr, 0);
    values.forEach((val, i) => {
      if (val !== 'null') {
        if (dTypes[i] === 'CHAR') {
          val = val.substring(1, val.length - 1);
        } else if (dTypes[i] === 'REAL') {
          val = parseFloat(val).toString();
        } else if (dTypes[i] === 'TIME') {
          val = formatEpochToDate(val);
        }
      }
      data[fieldIds[i]] = val;
    });
    parsedArx.data.push(data);
  });

  return parsedArx;
}

export default {
  parseArx
};
