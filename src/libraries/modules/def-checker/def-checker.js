const regexLib = {
  def: [
    {
      type: 'schema',
      listName: 'schemas'
    },
    {
      type: 'active link',
      listName: 'active_links'
    },
    {
      type: 'filter',
      listName: 'filters'
    },
    {
      type: 'container',
      listName: 'container',
      regexp: ' {3}type {11}: ',
      subtypes: [
        {
          type: '1',
          listName: 'al_guides'
        },
        {
          type: '2',
          listName: 'applications'
        },
        {
          type: '3',
          listName: 'packing_lists'
        },
        {
          type: '4',
          listName: 'filter_guides'
        },
        {
          type: '5',
          listName: 'web_services'
        }
      ]
    },
    {
      type: 'escalation',
      listName: 'escalations'
    },
    {
      type: 'char menu',
      listName: 'char_menus'
    },
    {
      type: 'distributed mapping',
      listName: 'distributed_mappings'
    },
    {
      type: 'distributed pool',
      listName: 'distributed_pools'
    },
    {
      type: 'Data Visualization Definition',
      listName: 'flashboard_plugin',
      regexp: ' {3}module {9}: Flashboard\r?\n {3}subtype {8}: ',
      subtypes: [
        {
          type: 'FLASHBOARD',
          listName: 'flashboards'
        },
        {
          type: 'ALARM',
          listName: 'flashboard_alarms'
        },
        {
          type: 'VARIABLE',
          listName: 'flashboard_variables'
        }
      ]
    },
    {
      type: 'image',
      listName: 'images'
    },
    {
      type: 'association',
      listName: 'associations'
    }
  ],
  pl: {
    regexps: [
      ' {3}datatype {7}: 0\r?\n {3}object {9}: ([^\r\n]*)\r?\n}',
      ' {3}datatype {7}: 1\r?\n {3}label {10}: ([^\r\n]*)\r?\n {3}value {10}: 0\\\\\r?\n}',
      ' {3}datatype {7}: 1\r?\n {3}value {10}: 0\\\\\r?\n}\r?\nreference {\r?\n {3}type {11}: 6\r?\n {3}datatype {7}: 0\r?\n {3}object {9}: ([^\r\n]*)\r?\n}'
    ],
    objects: [
      {
        type: '2',
        listName: 'schemas',
        regexp: 0
      },
      {
        type: '5',
        listName: 'active_links',
        regexp: 0
      },
      {
        type: '3',
        listName: 'filters',
        regexp: 0
      },
      {
        type: '32783',
        listName: 'al_guides',
        regexp: 2
      },
      {
        type: '32784',
        listName: 'applications',
        regexp: 2
      },
      {
        type: '32785',
        listName: 'packing_lists',
        regexp: 2
      },
      {
        type: '32793',
        listName: 'filter_guides',
        regexp: 2
      },
      {
        type: '32801',
        listName: 'web_services',
        regexp: 2
      },
      {
        type: '4',
        listName: 'escalations',
        regexp: 0
      },
      {
        type: '7',
        listName: 'char_menus',
        regexp: 0
      },
      {
        type: '32787',
        listName: 'distributed_mappings',
        regexp: 1
      },
      {
        type: '32792',
        listName: 'distributed_pools',
        regexp: 1
      },
      {
        type: '8',
        listName: 'images',
        regexp: 0
      },
      {
        type: '10',
        listName: 'associations',
        regexp: 0
      }
    ]
  }
};

RegExp.prototype.count = function (string) {
  let count = 0;

  while ((this.exec(string))) count++;

  return count;
};

function extractProp (objDefinitions, propName) {
  const regex = new RegExp(` {3}${propName} *: ([^\r\n]*)`);

  return regex.exec(objDefinitions);
}

function constructRegex (mode, ...objType) {
  const regexEnd = '\r?\nend(\r|\n|$)';
  let regexBegin,
    finalRegex,
    type;

  switch (mode) {
    case 'def':
      type = regexLib.def.filter(el => el.listName === objType[0]);

      if (typeof type !== 'undefined' && type.length > 0) {
        type = type[0];
        regexBegin = `begin ${type.type}\r?\n`;

        if (type.hasOwnProperty('subtypes')) {
          if (objType[1]) {
            let subtype = type.subtypes.filter(el => el.listName === objType[1]);
            if (typeof subtype !== 'undefined' && subtype.length > 0) {
              subtype = subtype[0];
              finalRegex = new RegExp(`${regexBegin}( {3}name {11}: ([^\r\n]*)\r?\n${type.regexp + subtype.type}\r?\n([\\s\\S]*?))${regexEnd}`, 'g');
            } else {
              throw new Error('Subtype "' + objType[1] + '" of type "' + objType[0] + '" doesn\'t exist');
            }
          } else {
            throw new Error('Subtype of type "' + objType[0] + '" is not provided');
          }
        } else {
          finalRegex = new RegExp(regexBegin + '([\\s\\S]*?)' + regexEnd, 'g');
        }
      } else {
        throw new Error('Type "' + objType[0] + '" doesn\'t exist');
      }
      break;
    case 'pl':
      type = regexLib.pl.objects.filter(el => el.listName === objType[0]);

      if (typeof type !== 'undefined' && type.length > 0) {
        type = type[0];
        regexBegin = `reference {\r?\n {3}type {11}: ${type.type}\r?\n`;

        finalRegex = new RegExp(regexBegin + regexLib.pl.regexps[type.regexp], 'g');
      } else {
        throw new Error('Type "' + objType[0] + '" doesn\'t exist');
      }
      break;
    default:
      throw new Error('Invalid mode');
  }

  return finalRegex;
}

function extractObjects (defStr) {
  const res = {};
  const searchFn = (regex, listName, ...subgroup) => {
    if (!regex.test(defStr)) return;
    regex.lastIndex = 0;

    res[listName] = regex.matchAll(defStr, subgroup, true);
  };

  regexLib.def.forEach(function (obj) {
    if (obj.hasOwnProperty('subtypes')) {
      obj.subtypes.forEach(subtype => {
        const regex = constructRegex('def', obj.listName, subtype.listName);
        searchFn(regex, subtype.listName, 1);
      })
    } else {
      const regex = constructRegex('def', obj.listName);
      searchFn(regex, obj.listName, 1);
    }
  });

  return res;
}

function countDefinitions (defStr, ...objType) {
  let count = 0;
  const searchFn = (regex) => {
    if (!regex.test(defStr)) return 0;
    regex.lastIndex = 0;

    return regex.count(defStr);
  };

  if (objType.length === 0) {
    regexLib.def.forEach(function (obj) {
      if (obj.hasOwnProperty('subtypes')) {
        obj.subtypes.forEach(subtype => {
          const regex = constructRegex('def', obj.listName, subtype.listName);
          count += searchFn(regex);
        })
      } else {
        const regex = constructRegex('def', obj.listName);
        count += searchFn(regex);
      }
    });
  } else {
    const regex = constructRegex('def', objType);
    count += searchFn(regex);
  }

  return count;
}

function extractNames (objDefinitions) {
  const names = {};
  const addName = (objName, definition) => {
    names[objName].push(extractProp(definition, 'name')[1]);
  };

  for (const obj in objDefinitions) {
    if (objDefinitions.hasOwnProperty(obj)) {
      const objName = obj;
      names[objName] = [];
      objDefinitions[objName].forEach(el => addName(objName, el));
    }
  }
  return names
}

function getPackingListContents (plDef) {
  const content = {};
  const searchFn = (regex, listName, ...subgroup) => {
    if (!regex.test(plDef)) return;
    regex.lastIndex = 0;

    content[listName] = regex.matchAll(plDef, subgroup, true)
  };

  regexLib.pl.objects.forEach(function (obj) {
    const regex = constructRegex('pl', obj.listName);
    searchFn(regex, obj.listName, 1);
  });

  return content;
}

function compareLists (defObjects, listObjects) {
  const result = {
    valid: true,
    status: 'ok',
    results: {}
  };
  const addObject = (objName, item) => {
    const plIndex = listObjects[objName].indexOf(item);
    if (plIndex >= 0) {
      if (!result.results[objName].objects.found) result.results[objName].objects.found = [];

      result.results[objName].objects.found.push(item);
      listObjects[objName][plIndex] = null;
    } else {
      if (result.status !== 'nok') result.status = 'nok';
      if (!result.results[objName].objects.not_expected) result.results[objName].objects.not_expected = [];

      result.results[objName].objects.not_expected.push(item);
      listObjects[objName][plIndex] = null;
    }
  };

  for (const obj in defObjects) {
    if (defObjects.hasOwnProperty(obj)) {
      const objName = obj;
      result.results[objName] = {
        quantity: {
          def: defObjects[objName].length,
          user_list: 0
        },
        objects: {}
      };

      if (listObjects[objName]) {
        result.results[objName].quantity.user_list = listObjects[objName].length;

        defObjects[objName].forEach(item => addObject(objName, item));
        defObjects[objName] = defObjects[objName].filter(Boolean);
        listObjects[objName] = listObjects[objName].filter(Boolean);
      } else {
        result.status = 'nok';
        result.results[objName].objects.not_expected = defObjects[objName];
      }
    }
  }

  Object.keys(listObjects).forEach((key) => (listObjects[key].length === 0) && delete listObjects[key]);

  if (Object.keys(listObjects).length > 0) {
    if (result.status !== 'nok') result.status = 'nok';

    for (const obj in listObjects) {
      if (listObjects.hasOwnProperty(obj)) {
        if (!result.results[obj]) {
          result.results[obj] = {
            quantity: {
            def: 0,
              user_list: listObjects[obj].length
            },
            objects: {}
          };
        }

        result.results[obj].objects.not_found = listObjects[obj];
      }
    }
  }

  return result;
}

export default {
  compareFiles (defFile, plFile) {
    const defContent = extractNames(extractObjects(defFile));

    if (countDefinitions(plFile) !== 1) {
      return {
        valid: false,
        message: 'DEF file is not a packing list or contains more than one object.'
      };
    }

    const listContent = getPackingListContents(extractObjects(plFile).packing_lists[0]);

    return compareLists(defContent, listContent);
  },

  compareDefCustomList (defFile, list) {
    const defContent = extractNames(extractObjects(defFile));
    const jsonCopy = src => {
      return JSON.parse(JSON.stringify(src));
    };

    return compareLists(defContent, jsonCopy(list));
  }
};
