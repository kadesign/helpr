export default {
  init () {
    RegExp.prototype.matchAll = function (string, subgroup = 0, sortResults = false) {
      const match = [];
      let found;

      while ((found = this.exec(string))) match.push(found[subgroup]);

      return sortResults ? match.sort() : match;
    };
  }
}
