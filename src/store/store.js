import Vue from 'vue';
import Vuex from 'vuex';
import router from '@/router';

import { tools } from '@/data/tools';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentTool: null,
    tools
  },
  mutations: {
    changeCurrentTool: (state, tool) => {
      state.currentTool = tool;
    }
  },
  getters: {
    tools (state) {
      return state.tools;
    }
  },
  actions: {
    updateHeadData ({ getters, commit }) {
      const routeName = router.currentRoute.name;
      if (routeName === 'home' || routeName === 'privacy-policy') {
        commit('changeCurrentTool', null);
        return;
      }

      const allTools = getters.tools;
      const currentTool = allTools.filter(tool => {
        return tool.path === router.currentRoute.name;
      });

      commit('changeCurrentTool', currentTool[0]);
    }
  }
  /*
    Separate modules are loading dynamically via store.registerModule().
    Please don't add modules here until its state should be available globally.
  */
})
