const namespaced = true;

const state = {
    defFile: null,
    listFile: {
        file: null,
        type: null
    },
    customList: [],
    checkIsRunning: false
};

const mutations = {
    clearState (state) {
        state.listFile.file = null;
        state.listFile.type = null;
        state.customList = [];
    },
    flush (state) {
        state.listFile.file = null;
        state.listFile.type = null;
        state.customList = [];
        state.defFile = null;
        state.checkIsRunning = false;
    },
    setFile (state, { file, type }) {
        switch (type) {
            case 'def':
                if (file) {
                    const fr = new FileReader();

                    fr.onload = function (e) {
                        state.defFile = e.target.result;
                    };

                    fr.readAsText(file);
                } else {
                    state.defFile = null;
                }
                break;
            case 'list': {
                if (file) {
                    const fr = new FileReader();

                    fr.onload = function (e) {
                        state.listFile.file = e.target.result;
                    };

                    fr.readAsText(file);
                    let ext = file.name.split('.');
                    ext = ext[ext.length - 1];
                    state.listFile.type = (ext && ext !== ext[0]) ? ext : null;
                } else {
                    state.listFile.file = null;
                    state.listFile.type = null;
                }
                break;
            }
            default:
                break;
        }
    },
    setCustomList (state, list) {
        state.customList = list.slice();
    },
    runCheck (state) {
        state.checkIsRunning = true;
    },
    stopCheck (state) {
        state.checkIsRunning = false;
    }
};

const getters = {
    stateIsClean (state) {
        return state.defFile === null && state.listFile.file === null && state.customList.length === 0;
    },
    defFileIsSet (state) {
        return state.defFile !== null;
    },
    objListIsSet (state) {
        return state.listFile.file !== null || state.customList.length > 0;
    },
    listType (state) {
        if (state.listFile.file !== null) {
            return 'pl';
        } else if (state.customList.length > 0) {
            return 'custom';
        } else {
            return null;
        }
    },
    customList (state) {
        return state.customList;
    },
    customObjCount (state) {
        let objectCount = 0;
        state.customList.forEach(item => {
            objectCount += item.values.length;
        });
        return objectCount;
    },
    defFile (state) {
        return state.defFile;
    },
    listFile (state) {
        return state.listFile.file;
    },
    preparedCustomList (state) {
        const requestObjList = {};
        state.customList.forEach(item => {
            requestObjList[item.name] = item.values;
        });
        return requestObjList;
    },
    preparedRequest (state) {
        const fData = new FormData();
        fData.append('def', state.defFile);
        if (state.listFile.file) {
            fData.append('objectList', state.listFile.file);
            fData.append('objectListType', state.listFile.type);
        } else {
            const requestObjList = {};
            state.customList.forEach(item => {
                requestObjList[item.name] = item.values;
            });
            const jsoned = JSON.stringify(requestObjList);
            fData.append('objectList', jsoned);
            fData.append('objectListType', 'custom');
        }
        return fData;
    },
    checkIsRunning (state) {
        return state.checkIsRunning;
    }
};

const actions = {
    clearState ({ commit, getters }) {
        if (!getters.stateIsClean) commit('clearState');
    },
    setFile ({ commit }, { file, type }) {
        commit('setFile', { file, type });
    },
    commitList ({ commit }, list) {
        commit('setCustomList', list);
    }
};

export default {
    namespaced,
    state,
    mutations,
    getters,
    actions
}
