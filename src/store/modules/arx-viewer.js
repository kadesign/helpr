const namespaced = true;

const state = {
  arxFile: null
};

const mutations = {
  flush (state) {
    state.arxFile = null;
  },
  setFile (state, { file }) {
    if (file) {
      const fr = new FileReader();

      fr.onload = function (e) {
        state.arxFile = e.target.result;
      };

      fr.readAsText(file, 'ISO-8859-1');
    } else {
      state.arxFile = null;
    }
  }
};

const getters = {
  arxFileIsSet (state) {
    return state.arxFile !== null;
  },
  arxFile (state) {
    return state.arxFile;
  }
};

const actions = {
  clearState ({ commit, getters }) {
    if (getters.arxFileIsSet) commit('flush');
  },
  setFile ({ commit }, file) {
    commit('setFile', file);
  }
};

export default {
  namespaced,
  state,
  mutations,
  getters,
  actions
};
