import Vue from 'vue';
import App from './App.vue';
import router from './router';
import meta from 'vue-meta';
import store from '@/store/store';

// Global components
import Header from './components/global/Header.vue';
import Footer from './components/global/Footer.vue';
import ComponentHeader from './components/global/ComponentHeader.vue';

// Helpers
import RegExpHelper from '@/libraries/utils/regexp-helper';

Vue.config.productionTip = false;

// Vue-meta config
Vue.use(meta, {
  keyName: 'metaInfo',
  attribute: 'data-vue-meta',
  ssrAttribute: 'data-vue-meta-server-rendered',
  tagIDKeyName: 'vmid'
});

// Import of global components
Vue.component('appHeader', Header);
Vue.component('appFooter', Footer);
Vue.component('appComponentHeader', ComponentHeader);

// Register helpers
RegExpHelper.init();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
