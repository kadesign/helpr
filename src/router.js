import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/privacy-policy',
      name: 'privacy-policy',
      component: () => import(/* webpackChunkName: "privacy-policy" */ './views/PrivacyPolicy.vue')
    },
    {
      path: '/def-checker',
      name: 'def-checker',
      component: () => import(/* webpackChunkName: "def-checker" */ './components/def-checker/Main.vue')
    },
    {
      path: '/arx-viewer',
      name: 'arx-viewer',
      component: () => import(/* webpackChunkName: "arx-viewer" */ './components/arx-viewer/Main.vue')
    }
  ]
});
